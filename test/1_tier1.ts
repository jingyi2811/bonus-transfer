import { ethers } from "hardhat";
import {expect} from "chai";

describe("ERC20", function () {
  let admin: any;
  let account1: any;
  let ERC20Mock: any;

  before(async function () {
    [admin, account1] = await ethers.getSigners();
    const erc20ContractFactory = await ethers.getContractFactory("MyToken");
    ERC20Mock = await erc20ContractFactory.deploy();
    await ERC20Mock.deployed();
  });

  describe("Transfer tier 1", function () {
    it("Should get correct bonus for 1st transfer of tier1", async function () {
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("1"))
      const adminBalance = await ERC20Mock.balanceOf(admin.address)
      const account1Balance = await ERC20Mock.balanceOf(account1.address)
      expect(ethers.utils.formatEther(adminBalance)).to.equal("10.0")
      expect(ethers.utils.formatEther(account1Balance)).to.equal("1.0")
    });

    it("Should get correct bonus for 2nd transfer of tier1", async function () {
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("10"))
      const adminBalance = await ERC20Mock.balanceOf(admin.address)
      const account1Balance = await ERC20Mock.balanceOf(account1.address)
      expect(ethers.utils.formatEther(adminBalance)).to.equal("10.0")
      expect(ethers.utils.formatEther(account1Balance)).to.equal("11.0")
    });

    it("Should get correct bonus for 3rd transfer of tier1", async function () {
      await ERC20Mock.connect(account1).transfer(admin.address, ethers.utils.parseUnits("11"))
      const adminBalance = await ERC20Mock.balanceOf(admin.address)
      const account1Balance = await ERC20Mock.balanceOf(account1.address)
      expect(ethers.utils.formatEther(adminBalance)).to.equal("21.0")
      expect(ethers.utils.formatEther(account1Balance)).to.equal("10.0")
    });
  });
});