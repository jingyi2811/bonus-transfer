import { ethers } from "hardhat";
import {expect} from "chai";

describe("ERC20", function () {
  let admin: any;
  let account1: any;
  let ERC20Mock: any;

  before(async function () {
    [admin, account1] = await ethers.getSigners();
    const erc20ContractFactory = await ethers.getContractFactory("MyToken");
    ERC20Mock = await erc20ContractFactory.deploy();
    await ERC20Mock.deployed();

    for(let i = 0; i < 310; i++) { // 10 + 20 + 40 + 80 + 160
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("0.1"))
    }

    const adminBalance = await ERC20Mock.balanceOf(admin.address)
    const account1Balance = await ERC20Mock.balanceOf(account1.address)

    expect(ethers.utils.formatEther(adminBalance)).to.equal("470.0")
    expect(ethers.utils.formatEther(account1Balance)).to.equal("31.0")
  });

  describe("Transfer tier 6", function () {
    it("Should get correct bonus for 1st transfer for tier 6", async function () {
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("1"))
      const adminBalance = await ERC20Mock.balanceOf(admin.address)
      const account1Balance = await ERC20Mock.balanceOf(account1.address)
      expect(ethers.utils.formatEther(adminBalance)).to.equal("469.5") // 470 + 0.5 - 1
      expect(ethers.utils.formatEther(account1Balance)).to.equal("32.0")
    });
  });
});