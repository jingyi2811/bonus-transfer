import { ethers } from "hardhat";
import {expect} from "chai";

describe("ERC20", function () {
  let admin: any;
  let account1: any;
  let ERC20Mock: any;

  before(async function () {
    [admin, account1] = await ethers.getSigners();
    const erc20ContractFactory = await ethers.getContractFactory("MyToken");
    ERC20Mock = await erc20ContractFactory.deploy();
    await ERC20Mock.deployed();

    for(let i = 0; i < 10; i++) {
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("1"))
    }

    const adminBalance = await ERC20Mock.balanceOf(admin.address)
    const account1Balance = await ERC20Mock.balanceOf(account1.address)

    //   0 - 1, 10 - 1
    //   9 - 2, 19 - 2
    //  18 - 3, 28 - 3
    //  27 - 4, 37 - 4
    //  36 - 5, 46 - 5
    //  45 - 6, 55 - 6
    //  54 - 7, 64 - 7
    //  63 - 8, 73 - 8
    //  72 - 9, 82 - 9
    // 81 - 10, 91 - 10

    expect(ethers.utils.formatEther(adminBalance)).to.equal("91.0")
    expect(ethers.utils.formatEther(account1Balance)).to.equal("10.0")
  });

  describe("Transfer tier 2", function () {
    it("Should get correct bonus for 1st transfer for tier 2", async function () {
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("1"))
      const adminBalance = await ERC20Mock.balanceOf(admin.address)
      const account1Balance = await ERC20Mock.balanceOf(account1.address)
      expect(ethers.utils.formatEther(adminBalance)).to.equal("95.0")
      expect(ethers.utils.formatEther(account1Balance)).to.equal("11.0")
    });

    it("Should get correct bonus for 3nd transfer for tier 2", async function () {
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("1"))
      const adminBalance = await ERC20Mock.balanceOf(admin.address)
      const account1Balance = await ERC20Mock.balanceOf(account1.address)
      expect(ethers.utils.formatEther(adminBalance)).to.equal("99.0")
      expect(ethers.utils.formatEther(account1Balance)).to.equal("12.0")
    });

    it("Should get correct bonus for 3rd transfer for tier 2", async function () {
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("1"))
      const adminBalance = await ERC20Mock.balanceOf(admin.address)
      const account1Balance = await ERC20Mock.balanceOf(account1.address)
      expect(ethers.utils.formatEther(adminBalance)).to.equal("103.0")
      expect(ethers.utils.formatEther(account1Balance)).to.equal("13.0")
    });
  });
});