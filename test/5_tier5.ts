import { ethers } from "hardhat";
import {expect} from "chai";

describe("ERC20", function () {
  let admin: any;
  let account1: any;
  let ERC20Mock: any;

  before(async function () {
    [admin, account1] = await ethers.getSigners();
    const erc20ContractFactory = await ethers.getContractFactory("MyToken");
    ERC20Mock = await erc20ContractFactory.deploy();
    await ERC20Mock.deployed();

    for(let i = 0; i < 150; i++) { // 10 + 20 + 40 + 80
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("1"))
    }

    const adminBalance = await ERC20Mock.balanceOf(admin.address)
    const account1Balance = await ERC20Mock.balanceOf(account1.address)

    expect(ethers.utils.formatEther(adminBalance)).to.equal("251.0")
    expect(ethers.utils.formatEther(account1Balance)).to.equal("150.0")
  });

  describe("Transfer tier 5", function () {
    it("Should get correct bonus for 1st transfer for tier 5", async function () {
      await ERC20Mock.connect(admin).transfer(account1.address, ethers.utils.parseUnits("1"))
      const adminBalance = await ERC20Mock.balanceOf(admin.address)
      const account1Balance = await ERC20Mock.balanceOf(account1.address)
      expect(ethers.utils.formatEther(adminBalance)).to.equal("250.625") // 251 + 0.625 - 1
      expect(ethers.utils.formatEther(account1Balance)).to.equal("151.0")
    });
  });
});