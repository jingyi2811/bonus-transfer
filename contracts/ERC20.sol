// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;

contract MyToken {
    mapping(address => uint256) public balanceOf;
    mapping(address => mapping (address => uint256)) public allowed;
    uint256 public totalSupply;

    string public constant name = "TEST";
    string public constant symbol = "TEST";
    uint8 public constant decimals = 18;

    mapping(uint => uint) public startBonus;
    mapping(uint => uint) public endBonus;
    mapping(uint => uint) public bonus;

    constructor() {
        _mint(address(this), 1000_000_000000000000000);
        _mint(msg.sender, 1_000_000000000000000);

        startBonus[1] = 1000_000_000000000000000;    // 1000
        endBonus[1] = 910_000_000000000000000;       // 910
        bonus[1] = 10_000_000000000000000;           // 10

        startBonus[2] = 900_000_000000000000000;     // 900
        endBonus[2] = 805_000_000000000000000;       // 805
        bonus[2] = 5_000_000000000000000;            // 5

        startBonus[3] = 800_000_000000000000000;     // 800
        endBonus[3] = 7025_00_000000000000000;       // 702.5
        bonus[3] = 25_00_000000000000000;            // 2.5

        startBonus[4] = 700_000_000000000000000;     // 700
        endBonus[4] = 60125_0_000000000000000;       // 601.25
        bonus[4] = 125_0_000000000000000;            // 1.25

        startBonus[5] = 600_000_000000000000000;     // 600
        endBonus[5] = 500625_000000000000000;        // 500.625
        bonus[5] = 625_000000000000000;              // 0.625

        startBonus[6] = 500_000_000000000000000;     // 500
        endBonus[6] = 5_00_000000000000000;          // 0.5
        bonus[6] = 5_00_000000000000000;             // 0.5
    }

    function transfer(address receiver, uint256 numTokens) public returns (bool) {
        _transfer(msg.sender, receiver, numTokens);

        if(numTokens > 0) {
            _distributeBonus();
        }

        return true;
    }

    function approve(address delegate, uint256 numTokens) public returns (bool) {
        allowed[msg.sender][delegate] = numTokens;
        return true;
    }

    function transferFrom(address owner, address buyer, uint256 numTokens) public returns (bool) {
        require(numTokens <= allowed[owner][msg.sender]);

        balanceOf[owner] = balanceOf[owner] - numTokens;
        allowed[owner][msg.sender] = allowed[owner][msg.sender] - numTokens;
        balanceOf[buyer] = balanceOf[buyer] + numTokens;

        if(numTokens > 0) {
            _distributeBonus();
        }

        return true;
    }

    function _mint(address account, uint256 amount) internal virtual {
        totalSupply = totalSupply + amount;
        balanceOf[account] = balanceOf[account] + amount;
    }

    function _transfer(address sender, address receiver, uint256 numTokens) public returns (bool) {
        balanceOf[sender] = balanceOf[sender] - numTokens;
        balanceOf[receiver] = balanceOf[receiver] + numTokens;
        return true;
    }

    function _distributeBonus() internal {
        for(uint i = 1; i <= 6; i++){
            if (
                startBonus[i] >= balanceOf[address(this)]  &&
                endBonus[i] <= balanceOf[address(this)]
            ) {
                _transfer(address(this), msg.sender, bonus[i]);
                return;
            }
        }
    }
}
